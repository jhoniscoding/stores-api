"""peiky URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from peiky_api import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'stores', views.StoreViewSet)
router.register(r'products', views.ProductViewSet)
router.register(r'sales', views.SaleViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    url(r'^docs/', include_docs_urls(title='Peiky API', public=False)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
