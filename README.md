# Peiky API
This is a Django-Rest-Framework project to manage stores, products and sales

# Requirements
* `django = "2.2.2"`
* `djangorestframework = "3.9.4"`
* `markdown = "3.1.1"`
* `coreapi = "2.3.3"`
* `django-filter = "2.1.0"`
* `djongo = "1.2.33"`
* `mongodb = 4.0.3`

# Installation
Clone the repo

`git clone https://gitlab.com/jhoniscoding/stores-api.git`

Go into the project folder

`cd peiky`

Create virtual environment and install libraries ([Pipenv](https://docs.pipenv.org/en/latest/) is required)

`pipenv sync`

Activate virtual environment

`pipenv shell`

Run the migration scripts

`python manage.py migrate`

Create a super user to use all the API features (`POST`, `PUT`, `DELETE`)

`python manage.py createsuperuser`

Start server

`python manage.py runserver 8000`

You can now open the API in your browser at [http://127.0.0.1:8000](http://127.0.0.1:8000) to start using the API.

The docs are exposed in [http://127.0.0.1:8000/docs/](http://127.0.0.1:8000/docs/).

If you use the Login control in the top right corner you'll also be able to add, create and delete Stores, Products, Sales and Users.