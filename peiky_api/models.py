from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils import timezone


class Store(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=255)
    email = models.CharField(max_length=255, unique=True)
    owner = models.ForeignKey(User, related_name="stores", on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Product(models.Model):
    STOCK = "stock"
    SOLD = "sold"
    STATUS_CHOICES = ((STOCK, "In stock"), (SOLD, "Sold"))

    store = models.ForeignKey(Store, related_name="products", on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10)
    barcode = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    price = models.FloatField()
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class Sale(models.Model):
    total = models.DecimalField(max_digits=16, decimal_places=2)
    store = models.ForeignKey(Store, related_name="sales", on_delete=models.CASCADE)
    products = models.ManyToManyField(Product)
    created_at = models.DateTimeField(default=timezone.now)
