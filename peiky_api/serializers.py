from django.contrib.auth.models import User
from rest_framework import serializers

from peiky_api.models import Store, Product, Sale


class UserSerializer(serializers.HyperlinkedModelSerializer):
    stores = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name="store-detail")

    class Meta:
        model = User
        fields = ('url', 'username', 'email', "stores")


class StoreSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Store
        fields = ("url", "name", "address", "email", "owner", "products", "sales", "created_at")


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Product
        fields = ("url", "store", "status", "barcode", "name", "price", "created_at")


class SaleSerializer(serializers.HyperlinkedModelSerializer):
    total = serializers.ReadOnlyField()
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Sale
        fields = ("url", "total", "store", "products", "created_at")
