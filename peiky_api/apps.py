from django.apps import AppConfig


class PeikyApiConfig(AppConfig):
    name = 'peiky_api'
