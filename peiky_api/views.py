from django.contrib.auth.models import User
# Create your views here.
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, permissions, status
from rest_framework.filters import SearchFilter
from rest_framework.response import Response

from peiky_api.models import Store, Product, Sale
from peiky_api.serializers import UserSerializer, StoreSerializer, ProductSerializer, SaleSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve users list

    create:
    Create a new user with the given params

    retrieve:
    Retrieve user info for the given id

    destroy:
    Delete user with the given id

    update:
    Update user info with the given params

    partial_update:
    Update user fields with the given params
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAdminUser,)


class StoreViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve stores list

    create:
    Create a new store with the given params

    retrieve:
    Retrieve store info for the given id

    destroy:
    Delete store with the given id

    update:
    Update store info with the given params

    partial_update:
    Update store fields with the given params
    """
    queryset = Store.objects.all().order_by("-created_at")
    serializer_class = StoreSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (SearchFilter, DjangoFilterBackend,)
    filterset_fields = ('name', 'email', 'address')
    search_fields = ('name', 'address', 'email')

    def get_queryset(self):
        return self.request.user.stores.all().order_by("-created_at")


class ProductViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve products list

    create:
    Create a new product with the given params

    retrieve:
    Retrieve product info for the given id

    destroy:
    Delete product with the given id

    update:
    Update product info with the given params

    partial_update:
    Update product fields with the given params
    """
    queryset = Product.objects.all().order_by("-created_at")
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (SearchFilter, DjangoFilterBackend,)
    filterset_fields = ('status', 'barcode', 'name')
    search_fields = ('status', 'barcode', 'name')


class SaleViewSet(viewsets.ModelViewSet):
    """
    list:
    Retrieve sales list

    create:
    Create a new sale with the given params

    retrieve:
    Retrieve sale info for the given id

    destroy:
    Delete sale with the given id

    update:
    Update sale info with the given params

    partial_update:
    Update sale fields with the given params
    """
    queryset = Sale.objects.all().order_by('-created_at')
    serializer_class = SaleSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        serializer.instance.total = sum([prod.price for prod in serializer.instance.products.all()])
        serializer.instance.save(update_fields=["total"])
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

